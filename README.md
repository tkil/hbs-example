# tkil/hbs-example
#### Repo: https://gitlab.com/tkil/hbs-example

### Summary
A simple HBS project, just run `npm install` and then `npm start`

### Technologies Used
* Webpack
* Handlebars (HBS)

### Prerequisites
Node is installed and by proxy, npm

### Installation
```
npm install
```

### Running Project
```
npm start
```

### Author
* Tyler Kilburn <tylerkilburn@gmail.com>
